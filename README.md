# latex-to-video

this tool creates a video from a git repository containing latex files. 

Starting from a Start Date, the tool checks out the version corresponding to the date, generate the full pdf, converts the pdf to image and create a montage.
Then it moves to the next TIME_DELTA (3 hours per default) and repeat the operation (if no change occured, the montage is copied from the last generated image)

montages can be assembled to a video to see the results with the following command executed from where the montages are created.

```bash
ffmpeg -pattern_type glob -i "these*.png" video.mp4
```



