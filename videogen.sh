RESOLUTION="3 hours"
TILE="21x8"
START_DATE="2017-06-01 00:00:00"
END_DATE="2017-12-20 00:00:00"
unset DATE
unset LAST_DATE
unset MD5_PREV
unset MD5
unset commit_prev
unset commit
unset d
pause() {
read -n1 -r -p "Press any key to continue..." key
}

d="$END_DATE"
while [ "$d" != "$START_DATE" ]; do
  d=$(date -d "$d $RESOLUTION ago" "+%Y-%m-%d %H:%M:%S")
  echo "checking for $d"
  commit=$(git rev-list -n 1 --before="$d" master)
  DATE=$(date -d "$d $RESOLUTION ago" "+%Y-%m-%d_%H:%M:%S")

  echo last commit for dat $DATE is $commit
  if [ "$commit" != "$commit_prev" ]; then
    echo "commit $s if different from commit $commit_prev"
    commit_prev=$commit
    git checkout $commit
    latexmk  -pdf -interaction=nonstopmode  ./these.tex > /dev/null 2>&1
    MD5=$(md5sum these.pdf)
    if [ "$MD5" != "$MD5_PREV" ]; then
      rm -rf *.png
      convert -verbose  these.pdf these-%03d.png  > /dev/null
      montage these-*.png -geometry 210x297\>+2+2 -tile $TILE  snap/these_$DATE.png  > /dev/null 2>&1
      echo new file snap/these_$DATE.png
      LAST_DATE=$DATE
      MD5_PREV=$MD5
      continue
    fi
  fi
  echo "commit $s is not different from commit $commit_prev"
  cp snap/these_$LAST_DATE.png snap/these_$DATE.png
done

