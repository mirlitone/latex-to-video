from datetime import date, datetime, time, timedelta
import pysrt
subs=pysrt.SubRipFile()

def get_text(adate):
  if adate < datetime.strptime("2017-09-20 00:00:00","%Y-%m-%d %H:%M:%S"):
    return "writing"
  elif adate < datetime.strptime("2017-10-01 00:00:00","%Y-%m-%d %H:%M:%S"):
    return "editing"
  elif adate < datetime.strptime("2017-11-13 00:00:00","%Y-%m-%d %H:%M:%S"):
    return "submitted"
  else:
    return "defended"

fps=25
start_datetime=datetime.strptime("2017-07-01 00:00:00","%Y-%m-%d %H:%M:%S")
end_datetime=datetime.strptime("2017-12-20 00:00:00","%Y-%m-%d %H:%M:%S")

frame_counter=0
while start_datetime < end_datetime:
    frame_counter+=1
    start_datetime+= timedelta(hours=3)
    item=pysrt.srtitem.SubRipItem()

    item.text="%s\n%s"%(get_text(start_datetime),start_datetime)
    item.start.milliseconds = frame_counter/fps*1000
    item.end.milliseconds = (frame_counter+1)/fps*1000

    subs.append(item)




subs.save('/home/nherbaut/tmp/video.srt', encoding='utf-8')

